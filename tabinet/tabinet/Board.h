#pragma once
#include<vector>
#include "Card.h"
#include "utilities.h"


class Board : InterfaceGameObject
{


public:

	static const int numberOfCardsOnTable = 4u;

public:


	Board() = default;

	std::vector<Card>& CardsOnBoard();

	Card GetCard(const std::string&) override;

	void RemoveCard(const std::string&) override;

private:

	std::vector<Card> m_cards;

};