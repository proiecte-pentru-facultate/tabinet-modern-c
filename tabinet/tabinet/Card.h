#pragma once
#include<iostream>
#include<cstdint>
#include<string>

class Card
{
public:

	const static uint8_t lowestCardValue = 2u;

	const static uint8_t highestCardValue = 14u;

	enum class cardSimbol:uint8_t
	{
		CLUBS = 0,
		SPADES = 1,
		DIAMONDS = 2,
		HEARTS = 3 
	};

	enum class cardColor:uint8_t
	{
		BLACK = 0,
		RED = 1
	};

public:

	Card() = default;

	Card(uint8_t,cardColor,cardSimbol);

	Card(const Card&);

	Card& operator = (const Card&);

	~Card();

public:

	bool operator == (const Card&) const;

	friend std::ostream& operator<<(std::ostream&, const Card&);

public:

	cardColor GetColor() const;

	cardSimbol GetSimbol() const;

	uint8_t GetValue() const;

	std::string GetId() const;

	int CalculatePointsOnCard() const;

private:

	std::string ConvertSimbolToString(cardSimbol) const;

	std::string ConvertColorToString(cardColor) const;

private:

	std::string m_id;

	uint8_t m_value : 1;

	cardColor m_color : 1;

	cardSimbol m_simbol : 1;
};