#pragma once
#include "Card.h"

class InterfaceGameObject
{
public:


	InterfaceGameObject(){}

	virtual Card GetCard(const std::string&) = 0;

	virtual void RemoveCard(const std::string&) = 0;

	~InterfaceGameObject(){}


};