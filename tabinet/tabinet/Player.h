#pragma once
#include<string>
#include<vector>
#include "Card.h"
#include "InterfaceGameObject.h"

class Player : InterfaceGameObject
{
public:

	Player() = default;

	Player(const std::string&);

public:

	Card GetCard(const std::string&) override;

	void AddCard(const Card&);

	void RemoveCard(const std::string&) override;

	void CalculcatePointsOfPlayer();

	int GetPoints() const;

	std::vector<Card> GetTotalCards();
	
	std::vector<Card>& GetCardsInHand();

	std::string& GetPlayerName();

public:

	friend std::istream& operator >> (std::istream&, Player&);

	friend std::ostream& operator << (std::ostream&, Player&);

	bool operator < (const Player& player1) const;

	bool operator == (const Player& player1) const;

private:

	int m_points;

	std::string m_name;

	std::vector<Card> m_totalCards;

	std::vector<Card> m_cardsInHand;
};