#include "Board.h"

std::vector<Card>& Board::CardsOnBoard()
{
	return m_cards;
}

Card Board::GetCard(const std::string&cardId)
{
	return * utilities::FindPositionOfCard(m_cards, cardId);
}

void Board::RemoveCard(const std::string& cardId)
{
	m_cards.erase(utilities::FindPositionOfCard(m_cards, cardId));
}
