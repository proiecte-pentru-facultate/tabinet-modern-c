#include "Game.h"
#include "Logger.h"


void Game::Run()
{
	Deck deck;

	Board board;

	Player playerTakeLastCard;

	SetupGame(deck, board);

	Logger::logMessage(Logger::Level::INFO, "CUM SA FOLOSESTI APLICATIA:\n"
		"Alege una sau mai multe carti de pe masa\n"
		"Daca vrei sa pasezi mana urmatorului jucator,tasteaza Q\n"
		"Daca vrei sa te opresti cand alegi carti de pe masa, tasteaza STOP\n");

	while (deck.GetCards().size() > 0)
	{
		int gameTurn = 0;

		for (auto& player : m_players)
		{

			std::string cardInPlayerHand = "";

			std::string choosenCard = "";

			std::vector<Card> cardsChoosedByPlayer;

			Logger::logMessage(Logger::Level::INFO, " Tura ", gameTurn, " Alege playerul : ", player);

			Logger::logMessage(Logger::Level::INFO, " Cartile tale ");

			utilities::DisplayCards(player.GetCardsInHand());

			Logger::logMessage(Logger::Level::INFO, " Carti pe masa ");

			utilities::DisplayCards(board.CardsOnBoard());

			Logger::logMessage(Logger::Level::INFO, " Alege o carte de la tine din mana ");

			std::cin >> choosenCard;

			cardInPlayerHand = choosenCard;

			if (cardInPlayerHand == "Q")
			{
				ZeroCardsPicked(player, board);
				continue;
			}

			Logger::logMessage(Logger::Level::INFO, " Alege carti de pe masa cu care sa faci pereche ");

			while (choosenCard != "STOP")
			{

				if (board.CardsOnBoard().size() == 0)
				{
					AddCardsOnBoard(board,deck);
				}

				std::cin >> choosenCard;

				if (choosenCard == "STOP") break;

				playerTakeLastCard = player;

				cardsChoosedByPlayer.push_back(board.GetCard(choosenCard));

				if (board.CardsOnBoard().size() == 0)
				{
					Logger::logMessage(Logger::Level::INFO, "Jucatorul ", player, "a facut o tabla!");
					std::cout << cardsChoosedByPlayer[utilities::GenerateRandomNumber(0, cardsChoosedByPlayer.size())];
					AddCardsOnBoard(board, deck);
				}
			}

			if (SumOfPlayerCards(cardsChoosedByPlayer) == player.GetCard(cardInPlayerHand).GetValue())
			{
				player.GetTotalCards().push_back(player.GetCard(cardInPlayerHand));

				for (auto& card : cardsChoosedByPlayer)
				{
					player.GetTotalCards().push_back(card);
					board.RemoveCard(card.GetId());
				}
			}
			else
			{
				Logger::logMessage(Logger::Level::WARNING, "Suma dintre cartea din mana si cele alese nu corespunde!", player);
			}

		}



		if (std::accumulate(begin(m_players), end(m_players), 0, [](int acc, Player& player)
		{
			return acc + player.GetCardsInHand().size();
		}) == 0)
		{
			int cardsPerPlayer = deck.GetCards().size() / m_players.size();

			if (cardsPerPlayer >= 6)
			{
				DealCardsToPlayers(deck, 6);
			}
			else
			{
				if (cardsPerPlayer >= 1)
				{
					DealCardsToPlayers(deck, cardsPerPlayer);
				}
				else
				{
					Logger::logMessage(Logger::Level::WARNING, "Nu mai sunt destule carti pentru fiecare player, stop joc !");
					break;
				}
			}
		}


		gameTurn++;
	}



	LastPlayer(board, playerTakeLastCard);

	DeclareTheWinner();

}

void Game::DealCardsToOnePlayer(Player &player,Deck& deck,const int noCards)
{
	for (auto cardNumber = 0u; cardNumber < noCards; ++cardNumber)
	{
		player.AddCard(deck.GetRandomCard());
	}
}

int Game::SumOfPlayerCards(const std::vector<Card>&playerCards)
{
	return std::accumulate(begin(playerCards), end(playerCards), 0, [&](uint8_t accumulator,const Card& two) {

		return accumulator + int(two.GetValue());

	});
}

void Game::ZeroCardsPicked(Player&player,Board&board)
{
	Logger::logMessage(Logger::Level::INFO,"Din moment ce nu ai luat nimic de pe masa , te rog sa lasi o carte jos");

	std::string choosenCard;

	std::cin >> choosenCard;

	board.CardsOnBoard().push_back(player.GetCard(choosenCard));

	player.RemoveCard(choosenCard);
}

void Game::DealCardsToPlayers(Deck& deck,const int cardsPerPlayer)
{
	for (auto& player : m_players)
	{
		DealCardsToOnePlayer(player, deck, cardsPerPlayer);
	}
}


void Game::LastPlayer(Board& board, Player&player) const
{
	if (board.CardsOnBoard().size() > 0)
	{
		for (auto& card : board.CardsOnBoard())
		{
			player.AddCard(card);

			board.RemoveCard(card.GetId());
		}
	}
}

void Game::AddCardsOnBoard(Board&board, Deck&deck) const
{
	for (auto index = 0u; index < Board::numberOfCardsOnTable; ++index)
	{
		board.CardsOnBoard().push_back(deck.GetRandomCard());
	}
}

bool Game::CheckIfPlayerExist(const Player& playerToCheck) const
{
	for (const auto& player : m_players)
	{
		if (player == playerToCheck) return true;
	}

	return false;
}


void Game::DeclareTheWinner() const 
{
	Player winnerPlayer = * begin(m_players);

	for (auto player = begin(m_players); player != end(m_players); ++player)
	{
		winnerPlayer = utilities::maxim(*player, winnerPlayer);
	}

	Logger::logMessage(Logger::Level::INFO, "Playerul castigator este : ", winnerPlayer);

}


Game& Game::GetInstance()
{
	static Game instance;

	return instance;
}

void Game::SetupGame(Deck &deck,Board &board)
{
	int noPlayers;

	Logger::logMessage(Logger::Level::INFO, "Se citeste numarul de playeri");

	std::cin >> noPlayers;

	Logger::logMessage(Logger::Level::INFO, "Se citeste numele playerilor");

	AddingPlayers(noPlayers);

	Logger::logMessage(Logger::Level::INFO, "Se amesteca cartile");

	deck.ShuffleCards();

	Player& firstPlayer = * begin(m_players);

	DealCardsToOnePlayer(firstPlayer, deck, 4);

	auto FindIfPlayerLikesCards = [](Player& firstPlayer)->bool
	{

		std::string resp;

		Logger::logMessage(Logger::Level::INFO, "Va plac cartile primite ? ", firstPlayer, " ? ");

		utilities::DisplayCards(firstPlayer.GetCardsInHand());

		std::cin >> resp;

		for (auto c : resp)
		{
			c = ::tolower(c);
		}

		return resp == "da" ? true : false;
	};

	if (FindIfPlayerLikesCards(firstPlayer))
	{
		DealCardsToOnePlayer(firstPlayer, deck, 2);

		for (auto playerIndex = begin(m_players) + 1; playerIndex != end(m_players); ++playerIndex)
		{
			DealCardsToOnePlayer(*playerIndex, deck, 6);
		}

		AddCardsOnBoard(board, deck);
	}
	else
	{
		auto cardsOfFirstPlayer = firstPlayer.GetCardsInHand();

		for (auto indexCard = begin(cardsOfFirstPlayer);
			indexCard != end(cardsOfFirstPlayer); ++indexCard)
		{
			auto cardId = indexCard->GetId();

			board.CardsOnBoard().push_back(firstPlayer.GetCard(cardId));

			firstPlayer.RemoveCard(cardId);
		}

		DealCardsToPlayers(deck, 6);
	}
}


void Game::AddingPlayers(const int playersNumber)
{
	for (auto i = 0u; i < playersNumber; ++i)
	{ 
		Player aux;

		std::cin >> aux;

		if (CheckIfPlayerExist(aux))
		{
			Logger::logMessage(Logger::Level::ERROR, "Playerul cu numele ",aux," exista deja, te rog alege alt nume!");

			i--;

			continue;
		}

		m_players.emplace_back(aux);
	}
}
