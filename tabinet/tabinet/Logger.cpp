#include "Logger.h"

std::string Logger::ConvertLevelToString(Level level)
{
	switch (level)
	{
	case Logger::Level::INFO:
		return "INFO";
	case Logger::Level::WARNING:
		return "WARNING";
	case Logger::Level::ERROR:
		return "ERROR";
	case Logger::Level::QUESTION:
		return "QUESTION";
	default:
		return "UNKNOWN MESSAGE TYPE";
	}
}
