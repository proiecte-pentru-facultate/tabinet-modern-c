#pragma once
#pragma warning(disable:4996)
#include<string>
#include<ctime>
#include<iostream>
#include<iomanip>


class Logger
{

public:


	enum class Level
	{
		INFO,
		WARNING,
		ERROR,
		QUESTION,
	};


	template<class ...Args>
	static void logMessage(const Level& level, Args&&...params)
	{

		std::time_t currentTime = std::time(nullptr);

		struct tm buf;

		std::cout << '[' << std::put_time(::localtime(&currentTime), "%Y-%m-%d") <<
			']' << '[' << ConvertLevelToString(level) << ']';

		((std::cout << ' ' << std::forward<Args>(params)), ...);

		std::cout << "\n";
	}

	static std::string ConvertLevelToString(Level level);

private:

};



