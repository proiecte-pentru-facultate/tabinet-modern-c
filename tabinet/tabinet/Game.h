#pragma once
#include<vector>
#include<algorithm>
#include<numeric>
#include "Player.h"
#include "Deck.h"
#include "Team.h"
#include "Player.h"
#include "Deck.h"
#include "Card.h"
#include "Board.h"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class Game
{
public:

	void Run();

	static Game& GetInstance();

private:

	void SetupGame(Deck&,Board&);

private:

	void AddingPlayers(const int);

	void DealCardsToOnePlayer(Player&, Deck&,const int);

	int SumOfPlayerCards(const std::vector<Card>&);

	void ZeroCardsPicked(Player&,Board&);

	void DealCardsToPlayers(Deck& , const int);

	void DeclareTheWinner() const;

	void LastPlayer(Board&,Player&) const;

	void AddCardsOnBoard(Board&, Deck&) const;

	bool CheckIfPlayerExist(const Player&) const;

private:

	Game() {}

	Game(Game const&) = delete;

	void operator = (Game const&) = delete;

private:

	std::vector<Player> m_players;

};