#include "Player.h"
#include"utilities.h"


Player::Player(const std::string&name):m_name(name),m_points(NULL)
{
}

Card Player::GetCard(const std::string& cardId)
{
	return * utilities::FindPositionOfCard(m_cardsInHand, cardId);
}

void Player::AddCard(const Card& c)
{
	m_cardsInHand.emplace_back(c);
}

void Player::RemoveCard(const std::string& cardId)
{
	m_cardsInHand.erase(utilities::FindPositionOfCard(m_cardsInHand,cardId));
}

void Player::CalculcatePointsOfPlayer()
{
	for (const auto& card : m_totalCards)
	{
		m_points += card.CalculatePointsOnCard();
	}
}

int Player::GetPoints() const
{
	return m_points;
}

std::vector<Card>& Player::GetCardsInHand()
{
	return m_cardsInHand;
}

std::vector<Card> Player::GetTotalCards()
{
	return m_totalCards;
}

std::string& Player::GetPlayerName()
{
	return this->m_name;
}

bool Player::operator < (const Player& player1) const
{
	return (this->GetPoints() < player1.GetPoints());
}

bool Player::operator==(const Player& player1) const
{
	return this->m_name == player1.m_name ? true : false;
}

std::istream& operator>>(std::istream& in, Player& player)
{
	std::cout << "Se citeste numele playerului!" << std::endl;

	in >> player.GetPlayerName();

	std::cout << std::endl;

	return in;
}

std::ostream& operator<<(std::ostream&os, Player&player)
{
	os << player.m_name;

	return os;
}
