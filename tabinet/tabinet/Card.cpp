#include "Card.h"

Card::Card(uint8_t value, cardColor color, cardSimbol simbol):m_value(value),m_color(color),m_simbol(simbol)
{
	this->m_id = std::to_string(value) + "-" + ConvertColorToString(color) + "-" + ConvertSimbolToString(simbol);
}

Card::Card(const Card& c)
{
	this->m_id = c.m_id;
	this->m_color = c.m_color;
	this->m_simbol = c.m_simbol;
	this->m_value = c.m_value;
}

Card& Card::operator=(const Card&c)
{
	this->m_id = c.m_id;
	this->m_color = c.m_color;
	this->m_simbol = c.m_simbol;
	this->m_value = c.m_value;
	return *this;
}

bool Card::operator==(const Card& c) const
{
	return this->GetId() == c.GetId();
}

std::string Card::ConvertSimbolToString(cardSimbol simbol) const
{
	switch (simbol)
	{
	case cardSimbol(0):
		return "CLUBS";
	case cardSimbol(1):
		return "SPADES";
	case cardSimbol(2):
		return "HEARTS";
	case cardSimbol(3):
		return "DIAMONDS";
	}
}

std::string Card::ConvertColorToString(cardColor color) const
{
	return color == cardColor(0) ? "BLACK" : "RED";
}

int Card::CalculatePointsOnCard() const
{
	if (m_value == 10 || m_value == 11 || m_value == 12 || m_value == 13) return 1;
	if (m_value == 10 && m_simbol == cardSimbol::DIAMONDS) return 2;
	if (m_value == 2 && m_simbol == cardSimbol::CLUBS) return 2;
	if (m_simbol == cardSimbol::CLUBS) return 1;
}

 Card::cardColor Card::GetColor() const
{
	return this->m_color;
}

Card::cardSimbol Card::GetSimbol() const
{
	return cardSimbol();
}

uint8_t Card::GetValue() const
{
	return this->m_value;
}

std::string Card::GetId() const
{
	return this->m_id;
}

Card::~Card()
{
}

std::ostream& operator<<(std::ostream& os, const Card& c)
{
	os << c.m_id;
	return os;
}
