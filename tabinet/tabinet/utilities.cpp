#include "utilities.h"
#include<memory>

std::vector<Card>::iterator utilities::FindPositionOfCard(
    std::vector<Card>& cards,const std::string& cardId)
{

    return std::find_if(begin(cards), end(cards), [&](Card& card) {
        return card.GetId() == cardId;
    });

}

int utilities::GenerateRandomNumber(int p1, int p2)
{
    std::random_device rd;

    std::mt19937 gen(rd());

    std::uniform_int_distribution<> distr(p1, p2);

    return distr(gen);
}

void utilities::DisplayCards(const std::vector<Card>& cards)
{
    for (const auto& card : cards)
    {
        std::cout << card << std::endl;
    }
}

Player utilities::maxim(const Player& firstPlayer, const Player& secondPlayer)
{
    return firstPlayer < secondPlayer ? firstPlayer : secondPlayer;
}

