#pragma once
#include<functional>
#include "Player.h"

class Team
{
public:
	Team() = default;

	Team(Player&, Player&);

private:
	Player m_playerOne;

	Player m_playerTwo;
};