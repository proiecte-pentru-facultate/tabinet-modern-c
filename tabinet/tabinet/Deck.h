#pragma once
#include<vector>
#include<iterator>
#include<memory>
#include<algorithm>
#include "Card.h"
#include "InterfaceGameObject.h"

class Deck : InterfaceGameObject
{
public:

	const static uint8_t noOfSimbols = 4u;

public:

	Deck();

	std::vector<Card> GetCards();
	
	Card GetCard(const std::string&) override;

	void RemoveCard(const std::string&) override;

	void ShuffleCards();

	Card GetRandomCard();

private:

	std::vector<Card> m_cards;

};