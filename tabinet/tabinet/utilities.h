#pragma once
#include <vector>
#include<random>
#include "Card.h"
#include "Player.h"
#include "Logger.h"

namespace utilities
{
	std::vector<Card>::iterator FindPositionOfCard(
		std::vector<Card>& cards,const std::string&);

	int GenerateRandomNumber(int p1, int p2);

	void DisplayCards(const std::vector<Card>&);

	Player maxim(const Player& firstPlayer, const Player& secondPlayer);


}