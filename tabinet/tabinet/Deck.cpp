#include "Deck.h"
#include <iterator>
#include "utilities.h"

Deck::Deck()
{
	for ( uint8_t simbolIndex = 0u; simbolIndex < Deck::noOfSimbols; ++simbolIndex)
	{
		for  (uint8_t cardValue = Card::lowestCardValue; cardValue <= Card::highestCardValue; ++cardValue)
		{
			Card::cardColor color = simbolIndex == 0 || simbolIndex == 1 ? Card::cardColor(0) : Card::cardColor(1);

			Card newCard(cardValue, color, Card::cardSimbol(simbolIndex));

			m_cards.emplace_back(newCard);
		}
	}
}

std::vector<Card> Deck::GetCards()
{
	return this->m_cards;
}

Card Deck::GetCard(const std::string&cardId)
{
	return * utilities::FindPositionOfCard(m_cards, cardId);
}

void Deck::RemoveCard(const std::string& cardId)
{
	m_cards.erase(utilities::FindPositionOfCard(m_cards,cardId));
}

void Deck::ShuffleCards()
{
	std::random_device rd;

	std::mt19937 g(rd());

	std::shuffle(begin(m_cards), end(m_cards),g);
}

Card Deck::GetRandomCard()
{
	Card card = m_cards.at(utilities::GenerateRandomNumber(0, m_cards.size() - 1));

	RemoveCard(card.GetId());

	return card;
}
